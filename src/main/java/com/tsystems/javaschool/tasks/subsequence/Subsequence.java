package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */

    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        return check(x, y);
    }

    private static boolean check(List a, List b){
        if((a == null)||(b == null))
            throw new IllegalArgumentException();
        try {
            int coin = 0;   //counts the number of equals elements of List x and List ys
            for( int x=0, offset=0; x<a.size();x++ ){
                for( int y = offset; y<b.size(); y++ ){
                    if( b.get(y) == a.get(x)){
                        coin++;
                        offset = y;
                        break;
                    }
                }
            }
            return (coin==a.size() ? true : false);
        } catch (RuntimeException e) {
            //e.printStackTrace();
            return false;
        }
    }
}
