package com.tsystems.javaschool.tasks.calculator;

import java.util.LinkedList;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public String evaluate(String statement) {
        return myEval(statement);
    }

    static int priority(char op) {
        switch (op) {
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
            case '%':
                return 2;
            default:
                return -1;
        }
    }

    static void processOperator(LinkedList<Double> st, char op) {
        Double r = st.removeLast();
        Double l = st.removeLast();
        switch (op) {
            case '+':
                st.add(l + r);
                break;
            case '-':
                st.add(l - r);
                break;
            case '*':
                st.add(l * r);
                break;
            case '/':
                if (r == 0)
                    throw new ArithmeticException(){};
                st.add(l / r);
                break;
            case '%':
                st.add(l % r);
                break;
        }
    }

    public static String myEval(String s) {
        try {
            LinkedList<Double> st = new LinkedList<Double>();   //stores operands
            LinkedList<Character> op = new LinkedList<Character>();//stores operators
            for (int i = 0; i < s.length(); i++) {
                char c = s.charAt(i);
                if (c == ' ')
                    continue;
                switch (c){
                    case '(':
                        op.add('(');
                        break;

                    case ')':
                        //calculating an expression between parentheses and removing an opening parenthesis
                        while (op.getLast() != '(')
                            processOperator(st, op.removeLast());
                        op.removeLast();
                        break;

                    default: {
                        if (c == '+' || c == '-' || c == '*' || c == '/' || c == '%') {
                            if(op.size()>0 && c == op.getLast())
                                throw new IllegalArgumentException();
                            else {
                                //checking priority of the operation. Parenthesis has priority -1
                                while (!op.isEmpty() && (priority(op.getLast()) >= priority(c)))
                                    processOperator(st, op.removeLast());
                                op.add(c);
                            }
                        } else {
                            //calculating the total length of a number
                            String operand = "";
                            while (i < s.length() && (Character.isDigit(s.charAt(i)) || s.charAt(i)=='.'))
                                operand += s.charAt(i++);
                            --i;
                            st.add(Double.parseDouble(operand));
                        }
                    }
                }
            }
            while (!op.isEmpty()) processOperator(st, op.removeLast());
            return (st.get(0)%1==0 ? String.valueOf(st.get(0).intValue()) : String.valueOf(st.get(0)));

        } catch (RuntimeException e){
            //e.printStackTrace();
            return null;
        }
    }

}
