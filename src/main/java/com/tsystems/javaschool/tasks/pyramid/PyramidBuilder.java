package com.tsystems.javaschool.tasks.pyramid;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param  inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    public int[][] buildPyramid(List<Integer> inputNumbers) {
        return buildTriangle(inputNumbers);
    }

    //output array size calculation and checking the base for completion
    private static int[] countSize(int listSize) throws CannotBuildPyramidException{
        int rows = 0;
        int isTriangleOK = 0;
        for(int x=0, y=1; x+y<=listSize; isTriangleOK=x+y, x=x+y, y++, rows++);
        if(isTriangleOK!=listSize)
            throw new CannotBuildPyramidException("Wrong Triangle");
        else
            return new int[] { rows, (2*rows-1) };
    }


    private static int[][] buildTriangle(List<Integer> list) throws CannotBuildPyramidException{
        if(!list.isEmpty()&&!list.contains(null)&&(list.size()<=(Integer.MAX_VALUE-8)))
        {
            ArrayList<Integer> workList = new ArrayList<>();
            workList.addAll(list);
            Collections.sort(workList);
            int[] sizes = countSize(list.size());
            int[][] resArray = new int[sizes[0]][sizes[1]];

            //filling the zero digits array with numbers in the order of a triangle
            for(int x=0, offset=0; x<resArray.length && !workList.isEmpty(); x++, offset=0)
                for(int  curPos=0; (curPos<resArray[1].length-1)&&(curPos<sizes[0]-1+x)&&!workList.isEmpty(); offset+=2){
                    if (x % 2 == 0) {
                        curPos = ((sizes[0]-1)-x+offset);
                        resArray[x][curPos] = workList.remove(0);
                    } else {
                        curPos = (sizes[0]-x-1+offset);
                        resArray[x][curPos] = workList.remove(0);
                    }
                }
            return resArray;
        }
        else throw new CannotBuildPyramidException("Input is incorrect");
        }

}
